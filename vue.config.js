const path = require('path')

require('dotenv').config()

module.exports = {
  chainWebpack: (config) => {
    config.resolve.alias.set('~', path.resolve(__dirname, 'src'))
    config.resolve.alias.set('vue$', 'vue/dist/vue.runtime.esm.js')
  },
  transpileDependencies: [
    'vuetify'
  ],
  css:{
    extract:false  
  }
}
