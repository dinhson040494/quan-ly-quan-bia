export const daysOfWeek = [
  'Chủ Nhật',
  'Thứ 2',
  'Thứ 3',
  'Thứ 4',
  'Thứ 5',
  'Thứ 6',
  'Thứ 7'
]

export const HTTP_STATUS = {
  NOT_FOUND: 404,
  SERVER_ERROR: 500,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  FOR_BIDDEN: 403
}

export const NUMBER_CONSTANT = {}

export const EVENT = {
  CLICK: 'click',
  SELECT: 'select',
  INPUT: 'input',
  CHECK: 'check',
  CHANGED: 'changed',
  RESIZE: 'resize',
  CLOSE: 'close',
  LOADING: 'loading'
}

export const DAY_MILISECONDS = 24 * 60 * 60 * 1000

export const COOKIE_TOKEN_EXPIRE = DAY_MILISECONDS * 7

export const ZERO = 0

export const EMPTY = ''

export const BLANK = ' '

export const SLUG = '-'

export const SLASH = '/'

export const EMPTY_ARRAY = []

export const HTTP = 'http'

export const I18N_ROUTE = {
  HOME: 'index',
  POST_NEW: 'dang-tin'
}

export const PAGNATION = {
  TOTAL_PAGE: 0,
  CURRENT_PAGE: 1,
  PAGE_SIZE: 1,
  PAGE_SHOW: 5
}

export const SORT_OPTIONS = {
  PRICE_ASC: 'priceasc'
}

export const TRANSACTION_STATUS = {
  CHO_XAC_NHAN: 1,
  THANH_CONG: 2,
  XAC_NHAN_THU_CONG: 3,
  HUY: 4
}

export const TRANSACTION_STATUS_COLOR = {
  CHO_XAC_NHAN: "#4F79E7",
  THANH_CONG: "#BF4FE7",
  XAC_NHAN_THU_CONG: "#40BEB7",
  HUY: "#DB4E4E",
}

export const PATH = {
  HOME: "/",
  REQUEST_DOCTOR: "/yeu-cau-kham",
  PATIENT: "/benh-nhan",
  MEDICINE: "/vi-thuoc",
  PRESCRIPTION_BASE: "/bai-thuoc",
  PRESCRIPTION: "/don-thuoc",
  WAITING_PRESCRIPTION: "/len-don",
  CREATE_OLD_PRESCRIPTION: "/tao-don-thuoc-cu",
  ACCOUNT: "/nguoi-dung",
  STATISTIC: "/thong-ke",
  APPOINTMENT_SCHEDULE: "/lich-hen-kham",
}

export const SESSION_STORAGE_KEY = {
  PRESCRIPTION_ID: "bds-prescription-id",
}

export const PRINT_CATEGORY = {
  DIEU_TRI: 0,
  DON_THUOC: 1,
  THANH_TOAN: 2,
  TEM_NHIET: 3
}

export const STEP_BY_STEP = [
  {
    path: PATH.HOME,
    content: `Tab tổng quan để xem dữ liệu gồm: Bệnh nhân, vị thuốc, yêu cầu khám, tồn kho...`
  },
  {
    path: PATH.REQUEST_DOCTOR,
    content: `<span class="text-xl mb-6">Tab đăng kí là nơi để thực hiện:</span><br>
      <span class="leading-8">
      + Tạo yêu cầu khám mới<br>
      + Chỉnh sửa yêu cầu khám<br>
      + Xem danh sách yêu cầu khám<br>
      + Xem chi tiết yêu cầu khám<br>
      <span><u>Note</u>: Chỉ dành cho admin, lễ tân, quản lí</span>
      </span>
    `
  },
  {
    path: PATH.PATIENT,
    content: `<span class="text-xl mb-6">Tab bệnh nhân là nơi để thực hiện:</span><br>
      <span class="leading-8">
      + Tạo bệnh nhân mới<br>
      + Chỉnh sửa thông tin bệnh nhân<br>
      + Xem danh sách bệnh nhân<br>
      + Xem chi tiết bệnh nhân<br>
      </span>
    `
  },
  {
    path: PATH.MEDICINE,
    content: `<span class="text-xl mb-6">Tab vị thuốc là nơi để thực hiện:</span><br>
      <span class="leading-8">
      + Tạo vị thuốc mới<br>
      + Chỉnh sửa vị thuốc<br>
      + Nhập thêm tồn kho vị thuốc<br>
      + Xem danh sách vị thuốc<br>
      + Xem chi tiết vị thuốc<br>
      + Tạo, xem, sửa nhãn cho vị thuốc<br>
      + Tạo, xem, sửa nhóm cho vị thuốc<br>
      <span><u>Note</u>: Chỉ dành cho admin, quản lí, bác sỹ</span>
      </span>
    `
  },
  {
    path: PATH.PRESCRIPTION_BASE,
    content: `<span class="text-xl mb-6">Tab bài thuốc là nơi để thực hiện:</span><br>
      <span class="leading-8">
      + Tạo bài thuốc mới<br>
      + Chỉnh sửa bài thuốc<br>
      + Xem danh sách bài thuốc<br>
      + Xem chi tiết bài thuốc<br>
      + Tạo, xem, sửa nhãn cho bài thuốc<br>
      + Tạo, xem, sửa nhóm cho bài thuốc<br>
      <span><u>Note</u>: Chỉ dành cho admin, bác sỹ, quản lí</span>
      </span>
    `
  },
  {
    path: PATH.PRESCRIPTION,
    content: `<span class="text-xl mb-6">Tab đơn thuốc là nơi để thực hiện:</span><br>
      <span class="leading-8">
      + Tạo mới đơn thuốc bởi bác sỹ<br>
      + Chỉnh sửa đơn thuốc<br>
      + Xem danh sách đơn thuốc<br>
      + Xem chi tiết đơn thuốc<br>
      <span><u>Note</u>: Chỉ dành cho admin, bác sỹ, quản lí, bốc thuốc, sắc thuốc</span>
      </span>
    `
  },
  {
    path: PATH.WAITING_PRESCRIPTION,
    content: `<span class="mb-6">Tab lên đơn là nơi thể hiện danh sách bệnh nhân đang cần được lên đơn thuốc</span><br>
      <span><u>Note</u>: Chỉ dành cho admin, bác sỹ, quản lí</span>
      </span>
    `
  },
  {
    path: PATH.CREATE_OLD_PRESCRIPTION,
    content: `<span class="mb-6">Tab tạo đơn cũ là tạo những đơn thuốc đã khám trước đó</span><br>
      <span><u>Note</u>: Chỉ dành cho bác sỹ</span>
      </span>
    `
  },
  {
    path: PATH.ACCOUNT,
    content: `<span class="text-xl mb-6">Tab người dùng là nơi để thực hiện:</span><br>
      <span class="leading-8">
      + Tạo người dùng trong hệ thống<br>
      + Chỉnh sửa thông tin người dùng<br>
      + Xem danh sách người dùng<br>
      + Xem chi tiết người dùng<br>
      <span><u>Note</u>: Chỉ dành cho admin</span>
      </span>
    `
  },
  {
    path: PATH.STATISTIC,
    content: `<span class="mb-6">Tab thống kê sẽ thể hiện các thông số như: số lượng bệnh nhân, số lượng đơn thuốc, doanh thu, số lượng vị thuốc, ... bằng đồ thị.</span><br>
      <span><u>Note</u>: Chỉ dành cho admin</span>
      </span>
    `
  }
]
