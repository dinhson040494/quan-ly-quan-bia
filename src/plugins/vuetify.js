import Vue from 'vue'
import Vuetify, { VToolbar } from 'vuetify/lib'
import VueMeta from 'vue-meta'

Vue.use(Vuetify, {
  components: {
    VToolbar
  },
})
Vue.use(VueMeta)

const opts = {
  theme: {
    themes: {
      light: {
        primary: '#FBB040',
        secondary: '#424242',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FB8C00',
      },
    },
  },
}

export default new Vuetify(opts)
