import Vue from 'vue'
// register the plugin on vue
import Toasted from 'vue-toasted'

const options = {
    position: 'top-right',
    action: {
        text: '✖',
        onClick: (_e, toastObject) => {
            toastObject.goAway(0)
        }
    }
}

Vue.use(Toasted, options)

export const $toasted = new Vue({}).$toasted 