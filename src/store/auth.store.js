export default {
  namespaced: true,
  state: () => ({
    accessTokenExpDate: null
  }),
  getters: {},
  mutations: {
    SET_ATOKEN_EXP_DATE(state, payload) {
      state.accessTokenExpDate = payload
    }
  },
  actions: {}
}
