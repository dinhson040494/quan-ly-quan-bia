import Vue from 'vue'
import Vuex from 'vuex'
import commonService from '~/services/common.service'

Vue.use(Vuex)

Vue.config.devtools = true

export default new Vuex.Store({
  state: {
    userProfile: null,
    roleId: null,
    qrImage: null,
    transactionId: null,
    accessTokenExpDate: null,
    stepByStep: {
      current: null,
    },
  },
  mutations: {
    setData(state, payload) {
      Object.keys(payload).forEach(key => {
        if (state[key] !== undefined) {
          state[key] = payload[key]
        }
      })
    },
    setStepGuide(state, payload) {
      state.stepByStep.current = payload
    }
  },
})
