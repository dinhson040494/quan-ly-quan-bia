import Vue from "vue";
import VueRouter from "vue-router";
import DefaultLayout from "~/layouts/default.vue";
import LoginLayout from "~/layouts/login.vue";
import ForBidden from "~/layouts/forbidden.vue";
import PageNotFound from "~/layouts/pagenotfound.vue";
import ServerError from "~/layouts/servererror.vue";
import Home from "~/pages/home.vue";
import Login from "~/pages/login.vue";
import cookies from "~/utils/cookie.js";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    component: DefaultLayout,
    children: [
      {
        path: "/",
        name: "home",
        component: Home,
        meta: {
          title: "Tổng quan | Approver",
          breadcrumbs: "Tổng quan",
          isAuth: true,
        },
      },
      {
        path: "/san-pham",
        name: "product",
        component: () => import("~/pages/product.vue"),
        meta: {
          isAuth: true,
        }
      },
      {
        path: "/cong-no",
        name: "dept",
        component: () => import("~/pages/dept.vue"),
        meta: {
          isAuth: true,
        }
      },
      {
        path: "/quan-ly-tai-khoan",
        name: "accountManagement",
        component: () => import("~/pages/accountManagement.vue"),
        meta: {
          isAuth: true,
        }
      },
      {
        path: "/doi-mat-khau-ngan-hang",
        name: "changePasswordBank",
        component: () => import("~/pages/changePasswordBank.vue"),
        meta: {
          isAuth: true,
        }
      },
      {
        path: "/dang-xy-ly",
        name: "processQR",
        component: () => import("~/pages/processQR.vue"),
        meta: {
          isAuth: true,
        }
      },
      {
        path: "/thay-doi-mat-khau",
        name: "changePassword",
        component: () => import("~/pages/changePassword.vue"),
        meta: {
          isAuth: true,
        }
      },
    ]
  },
  {
    path: "",
    component: LoginLayout,
    children: [
      {
        path: "dang-nhap",
        name: "Login",
        component: Login,
        meta: {
          title: "Đăng nhập",
          breadcrumbs: "Đăng nhập",
          breadcrumbs: "Tổng quan",
        },
      },
    ],
  },
  {
    path: "",
    component: ForBidden,
    children: [
      {
        path: "khong-co-quyen",
        name: "Forbidden",
        component: ForBidden,
        meta: {
          title: "Không có quyền truy cập",
          breadcrumbs: "Không có quyền truy cập",
          breadcrumbs: "Không có quyền truy cập",
        },
      },
    ],
  },
  {
    path: "",
    component: ServerError,
    children: [
      {
        path: "loi-xay-ra",
        name: "ServerError",
        component: ServerError,
        meta: {
          title: "Lỗi hệ thống",
          breadcrumbs: "Lỗi hệ thống",
          breadcrumbs: "Lỗi hệ thống",
        },
      },
    ],
  },
  {
    path: "",
    component: PageNotFound,
    children: [
      {
        path: "*",
        name: "PageNotFound",
        component: PageNotFound,
        meta: {
          title: "Không tìm thấy",
          breadcrumbs: "Không tìm thấy",
          breadcrumbs: "Không tìm thấy",
        },
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

router.beforeEach((to, _, next) => {
  const token = cookies.get("token")
  if (to.name === "Login" && token) {
    next({ name: "home" });
    return;
  }
  if (!to.meta.isAuth) {
    next();
    return;
  }
  if (to.meta.isAuth && token) {
    next();
  } else {
    next({ name: "Login" });
  }
});

export default router;
