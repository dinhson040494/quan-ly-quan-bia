export const ENDPOINT = {
  /* Authe & autho */
  API_LOGIN: '/auth/login',
  API_LOGOUT: '/auth/logout',
  PROFILE: '/users/me',


  /* Master data */
  TRANSACTION: '/transaction',
  TRANSACTION_CONTENT: '/assist/transaction-content',
  CREATE_QR: '/assist/create-qr',
  SYSTEM_STATUS: '/system-status',
  CANCEL_QR: '/assist/transaction-cancel',
  APPROVED_BY_HAND: '/assist/transaction-approved',

  /* Admin */
  ACCOUNTS: '/admin/users',
  SHOPS: '/admin/shops',
  CREATE_ACCOUNT: '/admin/create-user',
  BANK_INFO: '/admin/bank-info',
  LOCK_ACCOUNT: '/admin/user/deactive',
  RESET_PASSWORD_ACCOUNT: '/admin/user/reset-password',

  /* Change password */
  CHANGE_PASSWORD: '/user/update-password',
}
