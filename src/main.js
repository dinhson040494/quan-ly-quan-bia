import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'
import store from './store'
import { EVENT } from './utils/constants'
import 'vuetify/dist/vuetify.min.css'
import '@/assets/css/tailwind.css'
import '@/assets/css/index.scss'
import './plugins/vue-toast'
import './plugins/services'
import './plugins/pusher'
import './plugins/modal'

Vue.config.productionTip = false

const eventBus = new Vue()
Vue.prototype.$eventBus = eventBus
Vue.prototype.$loading = {
  start() {
    eventBus.$emit(EVENT.LOADING, true)
  },
  finish() {
    eventBus.$emit(EVENT.LOADING, false)
  }
}

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
