import { Http } from './http.init'

export default class BaseService {
  constructor(option){
    option = option || {}
    const isAuth = option && option.auth ? option.auth : false
    this.http = new Http({ auth: isAuth })
    return this
  }

  query(url, config = {}) {
    return this.http.get(url, config)
  }

  find(url, config = {}){
    return this.http.get(url, config)
  }

  create(url, data, config = {}) {
    return this.http.post(url, data, config)
  }

  update(url, data, config = {}) {
    return this.http.put(url, data, config)
  }

  delete(url, config = {}) {
    return this.http.delete(url, config)
  }

  request(config = {}){
    return this.http.request(config)
  }
}
