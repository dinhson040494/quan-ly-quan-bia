/**
 * HTTP request layer
 * if auth is required return patched axios instance(with access token in headers)
 * else return clear axios instance
 */

import axios from 'axios'
import { $toasted } from '@/plugins/vue-toast'
import { AuthService } from '@/services/auth.service'
import { ResponseWrapper, ErrorWrapper } from './util'
import { HTTP_STATUS } from '~/utils/constants'

export class Http {
  constructor(options) {
    this.isAuth = options && options.auth ? options.auth : false
    const baseURL = options && options.baseURL ? options.baseURL : process.env.VUE_APP_API_URL

    this.instance = axios.create({
      baseURL
    })

    return this.init()
  }

  init() {
    if (this.isAuth) {
      this.instance.interceptors.request.use(
        request => {
          request.headers.authorization = AuthService.getBearer()
          // if access token expired and refreshToken is exist >> go to API and get new access token
          if (
            AuthService.isAccessTokenExpired() &&
            AuthService.hasRefreshToken()
          ) {
            return AuthService.debounceRefreshTokens()
              .then(response => {
                AuthService.setBearer(response.data.accessToken)
                request.headers.authorization = AuthService.getBearer()
                return request
              })
              .catch(error => Promise.reject(error))
          } else {
            return request
          }
        },
        error => {
          return Promise.reject(error)
        }
      )
      this.instance.interceptors.response.use(
        function (response) {
          const { messages } = response.data
          if (messages) {
            let toastMessage = Array.isArray(messages) ? messages[0].msg : messages
            toastMessage = toastMessage.split('.').join('<br/>')
            $toasted.success(toastMessage).goAway(5000)
          }
          return response
        },
        function (error) {
          const code = parseInt(error.response && error.response.status)
          let toastMessage = 'Có lỗi xảy ra, vui lòng liên hệ người quản trị'
          if (code === HTTP_STATUS.UNAUTHORIZED) {
            AuthService.resetAuthData()
            window.location.href = window.location.origin + '/dang-nhap'
          }
          if (code === HTTP_STATUS.FOR_BIDDEN) {
            window.location.href = window.location.origin + '/khong-co-quyen'
          }
          if (code === HTTP_STATUS.BAD_REQUEST) {
            const { message } = error.response.data
            // toastMessage = Array.isArray(message) ? messages[0].msg : messages
            toastMessage = message;
            // toastMessage = toastMessage.split('.').join('<br/>')
            $toasted.error(toastMessage).goAway(5000)
          }
          return Promise.reject(error)
        }
      )
    } else {
      this.instance.interceptors.response.use(
        function (response) {
          return new ResponseWrapper(response, response.data.data)
        },
        function (error) {
          const code = parseInt(error.response && error.response.status)
          let toastMessage = 'Có lỗi xảy ra, vui lòng liên hệ người quản trị'
          if (code === HTTP_STATUS.UNAUTHORIZED) {
            window.location.href = window.location.origin + '/dang-nhap'
          }
          if (code === HTTP_STATUS.FOR_BIDDEN) {
            window.location.href = window.location.origin + '/khong-co-quyen'
          }
          if (code === HTTP_STATUS.BAD_REQUEST) {
            const { messages } = error.response.data
            toastMessage = Array.isArray(messages) ? messages[0].msg : messages
            toastMessage = toastMessage.split('.').join('<br/>')
            $toasted.error(toastMessage).goAway(5000)
          }
          return Promise.reject(new ErrorWrapper(error))
        }
      )
    }
        

    return this.instance
  }
}
